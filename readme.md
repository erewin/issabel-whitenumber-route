### Installation
This php apidaemon.php script catch outgoing calls from from-internal context and save unanswered numbers in DB
for futher dynamic route logic.
Dynamic route will ask whiteway.php?did=xxxxxxxxx for route

1.add new user apiwatch to manager_custom.conf

[apiwatch2]
secret = apiwatch2
deny=0.0.0.0/0.0.0.0
permit=127.0.0.1/255.255.255.0
read = call
write = call

1.1 do not forget to run "manager reload" in asterisk console

2. git clone https://bitbucket.org/erewin/issabel-whitenumber-route.git


3. add table to mysql:
CREATE TABLE `whitewaydid` (
  `did` varchar(64) DEFAULT NULL,
  `source` varchar(64) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `callid` varchar(128) DEFAULT '0'
);

4. (optional) use supervisor for control launched script


### STEP-BY-STEP
0. yum install asterisk16-curl
    0.1 systemctl restart asterisk
1. cd /var/lib/asterisk
2. git clone https://erewin@bitbucket.org/erewin/issabel-whitenumber-route.git
3. cd issabel-whitenumber-route
4. cp www/whiteway.php /var/www/html/whiteway.php
5. add to /etc/asterisk/manager_custom.conf: 

[apiwatch2]
secret = apiwatch2
deny=0.0.0.0/0.0.0.0 
permit=127.0.0.1/255.255.255.0 
read = call
write = call

6. connect to mysql with login and PASSWORD from /etc/issabelpbx.conf 
mysql asterisk -u asteriskuser -pPASSWORD

CREATE TABLE whitewaydid ( did varchar(64) DEFAULT NULL, source varchar(64) DEFAULT NULL, enabled tinyint(1) DEFAULT '0', callid varchar(128) DEFAULT '0' );

7. install supervisor by command "yum install supervisor"
add new file to directory /etc/supervisor.d/apidaemon.ini for centos

[program:apidaemon]
user = root
command = php apidaemon.php
directory = /var/lib/asterisk/agi-bin/issabel-whitenumber-route
numprocs = 1
autorestart = true
autostart = true
stdout_logfile = /var/log/apidaemon.log
stderr_logfile = /var/log/apidaemon-errors.log
stopwaitsecs = 60
startsecs=5
startretries=10000000

8. systemctl enable supervisord
9. supervisorctl start apidaemon
That is all. 



### Using

apidaemon.php script catching calls from extension (from-internal context) and save in DB source and did if call was unanswered.
whiteway.php is script for dynamic route feature of Issabel. it will result with source for asked did. if no did asked then it will return list of dids in DB

https://issabel.yooxy.ru/whiteway.php?did=${CALLERID(num)}

be note: issabel.yooxy.ru should have correct certificate.

whiteway.php will not return source if ext have state > 1. becuase only 0 and 1 state tell us that ext ready for recieve call.

Dynamic route should use ext name as parameters 
