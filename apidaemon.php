<?php
openlog("APIDAEMON",LOG_PID, LOG_LOCAL0);
// Make sure you include the composer autoload.
require 'vendor/autoload.php';

use PAMI\Client\Impl\ClientImpl;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;


//PREDEFINED VARS
//$url = "http://157.230.17.235/cp/api";
$url = "https://yooxy.ru/";
$start_call 		= array("Newchannel"); 	// start call incoming and outgoing through trunk
$end_call 		= array("Hangup");	// end calls (will apply only on calls that marked as started
$changestate_event 	= array("Newstate");	// set new state (ringing,answered) for started_calls;

$started_calls 		= array(); ///array for online calls.
$online_calls 		= array(); ///array for online calls.

include '/etc/issabelpbx.conf';

$mysqluser      = $amp_conf['AMPDBUSER'];
$mysqlpassword  = $amp_conf['AMPDBPASS'];
$mysqlhost      = $amp_conf['AMPDBHOST'];
$mysqldbname    = $amp_conf['AMPDBNAME'];

$dbh			= new PDO('mysql:host='.$mysqlhost.';dbname='.$mysqldbname, $mysqluser, $mysqlpassword);

$options = array(
    'host' 	=> '127.0.0.1',
    'scheme' 	=> 'tcp://',
    'port' 	=> 5038,
    'username' 	=> 'apiwatch2',
    'secret' 	=> 'apiwatch2',
    'connect_timeout' 	=> 10000,
    'read_timeout' 	=> 10000
);



try
{


        $client = new ClientImpl($options);

// Registering a closure

$client->registerEventListener(
function ($event) {
     global $events_main;
     global $start_call;
     global $started_calls;
     global $online_calls;
     global $changestate_event;
     global $end_call;
     global $dbh;

    //     var_dump($event);

    /// recieve call from external trunk
     $call = array();
//     print_r($started_calls);
//     print_r($online_calls);

     if ( in_array($event->getName(),$start_call) ) {
	 $call['callid'] = $event->getUniqueid();
	 $call['status'] = 'started';
//	 $call[] = $event->getName();
//	 $call[] = $event->getContext();
	 $call['dialed'] = $event->getExtension();
	 $call['callerid'] = $event->getCallerIDNum();
//	 $call[] = "\n";     

	    if(!$call['dialed'] || !$call['callerid'] ) 
		return 1;

	    if (preg_match("/from-trunk/",$event->getContext())) {
		
//		apisend($call);
        	echo implode("\n",$call);
		$started_calls[] = $event->getUniqueid();
	    }

    	    if (preg_match("/from-internal/",$event->getContext()) && $call['dialed'] != 's') {
		$sql = "insert into whitewaydid (callid,did,source) values 
			    ('".$call['callid']."','".$call['dialed']."','".$call['callerid']."')";
		$stmt = $dbh->query($sql);
//		apisend($call);
		syslog(LOG_INFO,"insert: callid: ".$call['callid']." from:".$call['callerid']." destination: ".$call['dialed']);
		echo implode("\n",$call);
		$started_calls[] = $event->getUniqueid();
	    }


	unset($call);
	
      }
     //////
     
     /// change status to ringing and to answered
     if (in_array($event->getName(),$changestate_event)) {
	 $call['callid'] = $event->getUniqueid();

	 switch ($event->getChannelState()) {
	    case '6':
		$call['status'] = 'answered';
		$online_calls[] = $event->getUniqueid();

		$sql = "update whitewaydid set enabled = false where callid = '".$call['callid']."'";
		$stmt = $dbh->query($sql);
		syslog(LOG_INFO,"answered: callid: ".$call['callid']."from:".$call['callerid']." destination: ".$call['dialed']);

		break;
	    case '4':
		$call['status'] = 'ringing';
		break;
	    case '5':
		$call['status'] = 'ringing';
		break;
	    case '7':
		$call['status'] = 'busy';
		break;
	    default:
		$call['status'] = $event->getChannelState();
//		$sql = "update whitewaydid set enabled = true where callid = '".$call['callid']."'";
//		$stmt = $dbh->query($sql);
		break;
	 }
	 
	 $call[] = "\n";

//	if (in_array($event->getUniqueid(),$started_calls)) 
//	    apisend($call);
            echo implode("\n",$call);

	unset($call);
    }
    ////

     /// end calls only if it started before
     if (in_array($event->getName(),$end_call)) {

	//check if call inside started
	if (!in_array($event->getUniqueid(),$started_calls)) 
	    return 1;

	unset($started_calls[array_search($event->getUniqueid(),$started_calls)]);

	 $call['callid']   = $event->getUniqueid();
	 $call['cause']    = $event->getCause();
	 $call['causetxt'] = str_replace(' ', '', $event->getCauseText());

	//remove call from online calls
	if (isset($online_calls) && in_array($event->getUniqueid(),$online_calls)) {
	    unset($online_calls[array_search($event->getUniqueid(),$online_calls)]);
	    $call['status_explain'] = $call['causetxt'];
	} else {
		$sql = "update whitewaydid set enabled = true where callid = '".$call['callid']."'";
		$stmt = $dbh->query($sql);
		syslog(LOG_INFO,"No answer: callid: ".$call['callid']);
	    $call['status_explain'] = "noanswered";
	    
	}

	 $call['status'] = "Ended";
	 //	 $call[] = "\n";

//	 apisend($call);
	 echo implode("\n",$call);
	

	unset($call);
      }
      ////
}

);

        $client->open();
        $time = time();
        while(true)//(time() - $time) < 60) // Wait for events.
        {
            usleep(1000); // 1ms delay
            // Since we declare(ticks=1) at the top, the following line is not necessary
            $client->process();
        }
        $client->close(); // send logoff and close the connection.
} catch (Exception $e) {
        echo $e->getMessage() . "\n";
}

function apisend($call) {
    global $url;
    $param_array = array();
    foreach ($call as $key => $value) {
	$param_array[] = $key."=".$value; 
    }
    
    $param_string = "?".implode("&",$param_array);
/*    
    $ch = curl_init($url.$param_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $html = curl_exec($ch);
    curl_close($ch);
*/
    print_r($call);
    echo "\n";
    //echo implode("\n",$call)."\n";
}

?>