<?php
openlog("Whitewaydid",LOG_PID, LOG_LOCAL0);

if (isset($_GET['did']))
    $did = $_GET['did'];

//$did = "0617050161";

require '/var/lib/asterisk/agi-bin/issabel-whitenumber-route/vendor/autoload.php';

use PAMI\Client\Impl\ClientImpl;
use PAMI\Message\Action\ExtensionStateAction;

include '/etc/issabelpbx.conf';


$mysqluser 	= $amp_conf['AMPDBUSER'];
$mysqlpassword 	= $amp_conf['AMPDBPASS'];
$mysqlhost 	= $amp_conf['AMPDBHOST'];
$mysqldbname 	= $amp_conf['AMPDBNAME'];

$options = array(
    'host'      => '127.0.0.1',
    'scheme'    => 'tcp://',
    'port'      => 5038,
    'username'  => 'apiwatch2',
    'secret'    => 'apiwatch2',
    'connect_timeout'   => 10000,
    'read_timeout'      => 10000
);

    $dbh = new PDO('mysql:host='.$mysqlhost.';dbname='.$mysqldbname, $mysqluser, $mysqlpassword);

if (isset($did)) { //did requested in GET. Result will be a source or 0.
    $a = new ClientImpl($options);
    $a->open();

    $sql = "select source from whitewaydid where locate('".$did."', `did`) > 0 and enabled = true limit 1";
    $stmt = $dbh->query($sql);

    syslog(LOG_INFO,"check $did ");
    $log = "incoming call: callerid $did";
    if ($row = $stmt->fetch()) {
    //  echo "source: ".$row['source']."\n";
        $me = $a->send(new ExtensionStateAction($row['source'], 'from-internal'));
    //  echo "status: ".$me->getKey('status')."\n";
        if ($me->getKey('status') < 2)
	    echo $row['source'];
    
	$log .= " status: ".$me->getKey('status')." source: ".$row['source'];
    }
    $log .= " :end";
    syslog(LOG_INFO, $log);
    
    $sql = "delete from whitewaydid where locate('".$did."', `did`) > 0";
    $stmt = $dbh->query($sql);
    syslog(LOG_INFO, "Row with did $did removed");
    return 0;
}


//if no $did present then show some table

$sql = "select did,source,enabled from whitewaydid";
$stmt = $dbh->query($sql);

echo '<table border=1px"><th>did</th><th>source</th><th>Enabled</th>';

while ($row = $stmt->fetch()) {
    echo '<tr>';
    echo "<td>".$row['did']."</td><td>".$row['source']."</td><td>".$row['enabled']."</td>";
    echo '</tr>';
}


return 0;

?>